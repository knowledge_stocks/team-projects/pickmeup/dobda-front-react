# 프로젝트 결과물

## 관련 자료 링크

- [결과물 시연사이트](https://jhlee-ac68u.asuscomm.com:25280/)
- [픽미업 소개자료(구글독스)](https://docs.google.com/presentation/d/1mchniWW3BNz8kLZQ00RgmpcoqwAGtA4k/edit)
- [픽미업 소개자료(미리캔버스)](https://www.miricanvas.com/v/1vwvzx)
- [프론트엔드](https://gitlab.com/knowledge_stocks/team-projects/pickmeup/pickmeup-front-react)
- [백엔드](https://gitlab.com/knowledge_stocks/team-projects/pickmeup/api-server)
- [DB](https://gitlab.com/knowledge_stocks/team-projects/pickmeup/db)

## 1. 로그인 화면 소개

![로그인 화면 소개](https://gitlab.com/mulcam-semi-team2/project-results/-/raw/main/resources/1.%20로그인%20화면%20소개.gif)

## 2. 판매자의 판매 가능 지역 설정하기

![판매자의 판매 가능 지역 설정하기](https://gitlab.com/mulcam-semi-team2/project-results/-/raw/main/resources/2.%20판매자%20판매%20가능%20지역%20설정.gif)

## 3. 메인화면 디자인 소개

![메인화면 디자인 소개](https://gitlab.com/mulcam-semi-team2/project-results/-/raw/main/resources/3.%20메인화면%20디자인%20소개.gif)

## 4. 상품 카테고리별 조회, 검색

![상품 카테고리별 조회, 검색](https://gitlab.com/mulcam-semi-team2/project-results/-/raw/main/resources/4.%20상품%20카테고리별%20조회,%20검색.gif)

## 5. 구매자 마이페이지

![구매자 마이페이지](https://gitlab.com/mulcam-semi-team2/project-results/-/raw/main/resources/5.%20구매자%20마이페이지.gif)

## 6. 판매 페이지

![판매 페이지](https://gitlab.com/mulcam-semi-team2/project-results/-/raw/main/resources/6.%20판매%20페이지.gif)

## 7. 장바구니에 넣고 구매하기

![장바구니에 넣고 구매하기](https://gitlab.com/mulcam-semi-team2/project-results/-/raw/main/resources/7.%20장바구니%20구매.gif)

## 8. 판매 페이지에서 바로 구매하기

![판매 페이지에서 바로 구매하기](https://gitlab.com/mulcam-semi-team2/project-results/-/raw/main/resources/8.%20바로%20구매.gif)

## 9. 리뷰 작성, 리뷰 리스트

![리뷰 작성, 리뷰 리스트](https://gitlab.com/mulcam-semi-team2/project-results/-/raw/main/resources/9.%20리뷰%20작성,%20리뷰%20리스트.gif)

## 10. 판매자 마이페이지

![판매자 마이페이지](https://gitlab.com/mulcam-semi-team2/project-results/-/raw/main/resources/10.%20판매자%20마이페이지.gif)

## 11. 품목 등록

![품목 등록](https://gitlab.com/mulcam-semi-team2/project-results/-/raw/main/resources/11.%20품목%20등록.gif)

## 12. 판매 페이지 등록

![판매 페이지 등록](https://gitlab.com/mulcam-semi-team2/project-results/-/raw/main/resources/12.%20판매%20페이지%20등록.gif)

## 13. 판매자 주문 상태 변경

![판매자 주문 상태 변경](https://gitlab.com/mulcam-semi-team2/project-results/-/raw/main/resources/13.%20판매자%20주문%20상태%20변경.gif)
